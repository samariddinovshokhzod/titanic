import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    age_by_title = {}
    
    for title, age in df.groupby('Name')['Age']:
        age_by_title[title] = int(age.median())
        
    df['Age'].fillna(df['Name'].apply(lambda name: age_by_title[name.split(', ')[0]]), inplace=True)
    
    missing_values = df['Age'].isna().groupby(df['Name'].apply(lambda name: name.split(', ')[0])).sum()
    
    res = []
    
    for title in ['Mr.', 'Mrs.', 'Miss.']:
        res.append((title, missing_values.get(title, 0), age_by_title[title]))

    return res